package ar.edu.meli.herenciaCuenta;

public class CuentaSueldo extends CuentaBancaria {

	private Integer limiteExtraccionGratis = 5;
	private Double costoExtraer = 15.0;

	public CuentaSueldo() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void extraer(Double montoExtraer) throws Exception {
		if (limiteExtraccionGratis == 0) {
			montoExtraer += costoExtraer;
		}
		if (montoExtraer >= getSaldo() + costoExtraer) {
			throw new Exception("El saldo es insuficiente");
		}
		this.saldo -= montoExtraer;
		this.limiteExtraccionGratis--;

	}

}
