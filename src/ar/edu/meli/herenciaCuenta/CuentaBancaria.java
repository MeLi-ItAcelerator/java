package ar.edu.meli.herenciaCuenta;

public abstract class CuentaBancaria {
	public Cliente titular = new Cliente();
	public Double saldo = 0.0;
	
	public CuentaBancaria() {}

	public CuentaBancaria(Double saldoInicial) {
		this.saldo = saldoInicial;
	}
	public void depositar(Double montoDepositar) {
		this.saldo += montoDepositar;
	}
	
	
	public void extraer(Double montoExtraer) throws Exception {
		if (montoExtraer >= getSaldo()) {
			throw new Exception("El saldo es insuficiente");
		}
		this.saldo -= montoExtraer;
		
	}
	
	public Double getSaldo() {
		return this.saldo;
	}
}
