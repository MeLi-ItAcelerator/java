package ar.edu.meli.poliEstacionamiento;

public class AttrCargas{

	private Double cargaActual = 0.0;
	private Double cargaMaxima = 0.0;


	public AttrCargas(Double cargaMaxima) {
		this.cargaMaxima = cargaMaxima;
	}
	
	public Double getCargaMaxima() {
		return this.cargaMaxima;
	}
	
	public Double getCargaActual() {
		return this.cargaActual;
	}
	
	public void Cargar(Double carga) {
		this.cargaActual += carga;
	}
}
