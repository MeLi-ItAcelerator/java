package ar.edu.meli.poliEstacionamiento;

import java.util.LinkedList;

public class PlayaDeCarga {
	Double cargaDespachada = 0.0;
	Double coleccionCarga[] = { 120.0, 123.0, 432.0, 56.0, 78.0 };
	
	LinkedList<Cargable> cargados = new LinkedList<Cargable>();
	
	private PlayaDeCarga() {
	}

	private static PlayaDeCarga unica = new PlayaDeCarga();
	
	public static PlayaDeCarga getInstance() {
		return unica;
	}
	
	public void verSinCarga() {
	}
	
	public void cargarCargable(Cargable cargable) {
		cargados.add(cargable);
	}
	
	public void cargar() {
		Cargable primero = cargados.removeFirst();
		
		if (primero.getCargaMaxima() >= primero.getCargaActual() + coleccionCarga[0]) {
			primero.cargar(coleccionCarga[0]);
		}
		
		if (primero.getCargaMaxima() != primero.getCargaActual()) {
			cargados.addLast(primero);
		}
		
	}
	
	
	
	

}
