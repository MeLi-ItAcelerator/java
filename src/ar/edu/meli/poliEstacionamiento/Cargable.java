package ar.edu.meli.poliEstacionamiento;

public interface Cargable {
	void cargar(Double carga);
	Double getCargaMaxima();
	Double getCargaActual();
}
