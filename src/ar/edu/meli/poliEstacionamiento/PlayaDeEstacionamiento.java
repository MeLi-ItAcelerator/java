package ar.edu.meli.poliEstacionamiento;

import java.util.LinkedList;

public class PlayaDeEstacionamiento {
	LinkedList<Estacionable> estacionados = new LinkedList<Estacionable>();

	private PlayaDeEstacionamiento() {
	}

	private static PlayaDeEstacionamiento unica = new PlayaDeEstacionamiento();
	
	public static PlayaDeEstacionamiento getInstance() {
		return unica;
	}	
	
	public void recibirEstacionable(Estacionable estacionable) {
		estacionados.add(estacionable);
	}

	public Integer getEstacionados() {
		return estacionados.size();
	}
	
	public void vaciarPlaya(){
		estacionados.clear();
	}
}
