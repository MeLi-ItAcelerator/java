package ar.edu.meli.ejCerradura;

public class Cerradura {

	private Integer claveDeApertura;
	private Integer cantidadDeFallosConsecutivosQueLaBloquean;
	private Boolean puertaAbierta;
	private Integer fallosConsecutivosActuales=0, aperturasExitosas=0, AperturasFallidas=0;

	public Cerradura(int claveDeApertura, int cantidadDeFallosConsecutivosQueLaBloquean) {
		setClaveDeApertura(claveDeApertura);
		setCantidadDeFallosConsecutivosQueLaBloquean(cantidadDeFallosConsecutivosQueLaBloquean);
		setPuertaAbierta(true);
	}

	public Cerradura() {
		this(888, 3);
	}

	public Integer getAperturasExitosas() {
		return this.aperturasExitosas;
	}

	public void setAperturasExitosas(Integer aperturasExitosas) {
		this.aperturasExitosas = aperturasExitosas;
	}

	public Integer getAperturasFallidas() {
		return this.AperturasFallidas;
	}

	public void setAperturasFallidas(Integer aperturasFallidas) {
		this.AperturasFallidas = aperturasFallidas;
	}

	public Boolean getPuertaAbierta() {
		return this.puertaAbierta;
	}

	public void setPuertaAbierta(Boolean puertaAbierta) {
		this.puertaAbierta = puertaAbierta;
	}

	public Integer getClaveDeApertura() {
		return this.claveDeApertura;
	}

	public void setClaveDeApertura(Integer claveDeApertura) {
		this.claveDeApertura = claveDeApertura;
	}

	public Integer getCantidadDeFallosConsecutivosQueLaBloquean() {
		return this.cantidadDeFallosConsecutivosQueLaBloquean;
	}

	public void setCantidadDeFallosConsecutivosQueLaBloquean(Integer cantidadDeFallosConsecutivosQueLaBloquean) {
		this.cantidadDeFallosConsecutivosQueLaBloquean = cantidadDeFallosConsecutivosQueLaBloquean;
	}

	public Integer getFallosConsecutivosActuales() {
		return fallosConsecutivosActuales;
	}

	public void setFallosConsecutivosActuales(Integer fallosConsecutivosActuales) {
		this.fallosConsecutivosActuales = fallosConsecutivosActuales;
	}

	public boolean estaAbierta() {
		return this.getPuertaAbierta();
	}

	public boolean abrir(int clave) {
		Integer aux;
		if (this.estaAbierta()) {
			return true;
		}

		if (this.getClaveDeApertura().equals(clave)) {
			this.setPuertaAbierta(true);
			aux = this.getAperturasExitosas();
			aux = aux + 1;
			this.setAperturasExitosas(aux);
			return true;
		}
		aux = this.getAperturasFallidas();
		aux = aux + 1;

		this.setAperturasFallidas(aux);
		return false;
	}

	public boolean fueBloqueada() {
		if (this.getCantidadDeFallosConsecutivosQueLaBloquean() >= this.getFallosConsecutivosActuales()) {
			return true;
		}
		return false;
	}

	public int contarAperturasExitosas() {
		return getAperturasExitosas();
	}

	public int contarAperturasFallidas() {
		return this.getAperturasFallidas();
	}

	public boolean estaCerrada() {
		return this.getPuertaAbierta();
	}

	public void cerrar() {
		this.setPuertaAbierta(false);
	}

}