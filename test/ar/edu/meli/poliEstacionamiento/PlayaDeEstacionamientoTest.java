package ar.edu.meli.poliEstacionamiento;

import static org.assertj.core.api.Assertions.*;
import org.junit.Test;

public class PlayaDeEstacionamientoTest {
	PlayaDeEstacionamiento pl = PlayaDeEstacionamiento.getInstance();

	@Test
	public void alCrearUnaPlayaDeEstacionamientoLaMismaEstaVacia() {
		assertThat(pl.getEstacionados() == 0).isTrue();
		pl.vaciarPlaya();
	}

	@Test
	public void alCargarAutosLaCantidadDeberiaCoincidir() {
		Autos auto = new Autos();
		auto.estacionar();
		assertThat(pl.getEstacionados() == 1).isTrue();
		pl.vaciarPlaya();

	}

	@Test
	public void alVaciarDeberiaVaciar() {

		Camionetas camioneta = new Camionetas(500.0);
		for (int i = 0; i < 5; i++) {
			pl.recibirEstacionable(camioneta);
		}
		pl.vaciarPlaya();

		assertThat(pl.getEstacionados() == 0).isTrue();

	}

}
