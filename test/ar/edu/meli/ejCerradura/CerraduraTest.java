package ar.edu.meli.ejCerradura;

import junit.framework.TestCase;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class CerraduraTest {
	Cerradura cerradura = new Cerradura();

	@Test
	public void alCrearLaCerraduraDeberiaEstarAbierta() {
		assertThat(cerradura.estaAbierta()).isTrue();
	}

	@Test
	public void alIntentarAbrirConClaveCorrectaDeberiaAbrir() {
		int clave = 888;
		assertThat(cerradura.abrir(clave)).isTrue();
	}

	@Test
	public void alCerrarDeberiaCerrar() {
		cerradura.cerrar();
		assertThat(cerradura.getPuertaAbierta()).isFalse();
	}

	@Test
	public void alContarAperturasExitosasDeberiaDarIgual() {
		int clave = 888;
		cerradura.cerrar();
		cerradura.abrir(clave);
		cerradura.cerrar();
		cerradura.abrir(clave);
		assertThat(cerradura.contarAperturasExitosas() == 2).isTrue();

	}

	@Test
	public void alIntentarAbrirConClaveErroneaDeberiaNoAbrir() {
		int clave = 123;
		cerradura.cerrar();
		assertThat(cerradura.abrir(clave)).isFalse();
	}

}