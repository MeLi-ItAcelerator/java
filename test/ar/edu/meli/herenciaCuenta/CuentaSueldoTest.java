package ar.edu.meli.herenciaCuenta;

import static org.assertj.core.api.Assertions.*;
import org.junit.Test;

public class CuentaSueldoTest {

	CuentaSueldo miCuenta = new CuentaSueldo();

	@Test
	public void agregarDineroCuentaSueldoDeberiaDepositar() {

		Double saldoActual;
		saldoActual = miCuenta.getSaldo();
		Double dinero = 100.0;
		miCuenta.depositar(dinero);

		assertThat(miCuenta.getSaldo() == (saldoActual + dinero)).isTrue();
	}

	@Test(expected = Exception.class)
	public void quitarMasDineroCuentaSueldoDelQueTengoDeberiaNoExtraer() throws Exception {
		miCuenta.depositar(100.0);
		miCuenta.extraer(200.0);
	}

	@Test
	public void quitarrDineroCuentaSueldoDeberiaExtraer() throws Exception {

		miCuenta.depositar(150.0);
		Double saldoActual;
		saldoActual = miCuenta.getSaldo();
		Double dinero = 100.0;
		miCuenta.extraer(dinero);

		assertThat(miCuenta.getSaldo() == (saldoActual - dinero)).isTrue();
	}
	@Test
	public void quitarMasDeCincoExtracciones() throws Exception {
		
	}
	
	
}
